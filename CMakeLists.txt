#
# CMakeLists.txt  top-level cmake file for pufferscale system
# 19-Jul-2018
#

#
#  general cmake flags:
#    -DCMAKE_INSTALL_PREFIX=/usr/local     -- the prefix for installing
#    -DCMAKE_BUILD_TYPE=type               -- type can be Debug, Release, ...
#    -DCMAKE_PREFIX_PATH=/dir              -- external packages
#
#     note that CMAKE_PREFIX_PATH can be a list of directories:
#      -DCMAKE_PREFIX_PATH='/dir1;/dir2;/dir3'
#

cmake_minimum_required (VERSION 3.0)
project (Pufferscale-hepnos C CXX)
enable_testing ()

macro (use_cxx14)
  if (CMAKE_VERSION VERSION_LESS "3.1")
    if (CMAKE_CXX_COMPILER_ID STREQUAL "GNU")
      set (CMAKE_CXX_FLAGS "-rdynamic --std=gnu++14 -g ${CMAKE_CXX_FLAGS}")
    else ()
      set (CMAKE_CXX_FLAGS "-rdynamic --std=c++14 -g ${CMAKE_CXX_FLAGS}")
    endif ()
  else ()
    set (CMAKE_CXX_STANDARD 14)
  endif ()
endmacro (use_cxx14)

# Set module path
set(CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} 
		"${Pufferscale-hepnos_SOURCE_DIR}/cmake/Modules/")

# Find Yaml-CPP
find_package(YamlCpp)
if(YAMLCPP_FOUND)
	include_directories(${YAMLCPP_INCLUDE_DIR})
else(YAMLCPP_FOUND)
	message(ERROR "YamlCpp not found")
endif(YAMLCPP_FOUND)


use_cxx14 ()

find_package(MPI)
include_directories(SYSTEM ${MPI_INCLUDE_PATH})

# set (CMAKE_CXX_FLAGS "-o3 -DNDEBUG -Wall -Wextra ${CMAKE_CXX_FLAGS}")
set (CMAKE_CXX_FLAGS "-rdynamic -Wall -Wextra ${CMAKE_CXX_FLAGS}")

# add our cmake module directory to the path
set (CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH}
     "${CMAKE_CURRENT_SOURCE_DIR}/cmake")

# link shared lib with full rpath
set (CMAKE_INSTALL_RPATH "${CMAKE_INSTALL_PREFIX}/lib")
set (CMAKE_INSTALL_RPATH_USE_LINK_PATH TRUE)

# setup cache variables for ccmake
if (NOT CMAKE_BUILD_TYPE)
    set (CMAKE_BUILD_TYPE Release
         CACHE STRING "Choose the type of build." FORCE)
    set_property (CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS 
                  "Debug" "Release" "RelWithDebInfo" "MinSizeRel")
endif ()
set (CMAKE_PREFIX_PATH "" CACHE STRING "External dependencies path")
set (BUILD_SHARED_LIBS "OFF" CACHE BOOL "Build a shared library")

# packages we depend on
find_package (thallium CONFIG REQUIRED)
find_package (Pufferscale)
include (xpkg-import)
xpkg_import_module (margo REQUIRED margo)
xpkg_import_module (sdskv-client REQUIRED sdskv-client)
xpkg_import_module (sdskv-server REQUIRED sdskv-server)
xpkg_import_module (abt-io REQUIRED abt-io)

add_subdirectory (src)
