#
# Find the CppUnit includes and library
#
# This module defines
# CPPUNIT_INCLUDE_DIR, where to find tiff.h, etc.
# CPPUNIT_LIBRARIES, the libraries to link against to use CppUnit.
# CPPUNIT_FOUND, If false, do not try to use CppUnit.

# also defined, but not for general use are
# CPPUNIT_LIBRARY, where to find the CppUnit library.
# CPPUNIT_DEBUG_LIBRARY, where to find the CppUnit library in debug mode.


FIND_PATH(PUFFERSCALE_INCLUDE_DIR pufferscale/Controller.hpp HINTS
		/usr/local/include
		/usr/include
		ENV pufferscale_ROOT
	 )

# On unix system
FIND_LIBRARY(PUFFERSCALE_LIBRARY pufferscale
			${PUFFERSCALE_INCLUDE_DIR}/../lib
			/usr/local/lib
			/usr/lib)

IF(PUFFERSCALE_INCLUDE_DIR)
	IF(PUFFERSCALE_LIBRARY)
		MESSAGE(STATUS "Pufferscale found")
		SET(PUFFERSCALE_FOUND "YES")
		SET(PUFFERSCALE_LIBRARIES ${PUFFERSCALE_LIBRARY} ${CMAKE_DL_LIBS})
	ENDIF(PUFFERSCALE_LIBRARY)
ENDIF(PUFFERSCALE_INCLUDE_DIR)
