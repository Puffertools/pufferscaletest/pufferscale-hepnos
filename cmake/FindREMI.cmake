#
# Find the CppUnit includes and library
#
# This module defines
# CPPUNIT_INCLUDE_DIR, where to find tiff.h, etc.
# CPPUNIT_LIBRARIES, the libraries to link against to use CppUnit.
# CPPUNIT_FOUND, If false, do not try to use CppUnit.

# also defined, but not for general use are
# CPPUNIT_LIBRARY, where to find the CppUnit library.
# CPPUNIT_DEBUG_LIBRARY, where to find the CppUnit library in debug mode.


FIND_PATH(REMI_INCLUDE_DIR remi/remi-common.h HINTS
		/usr/local/include
		/usr/include
		ENV remi_ROOT
	 )

# On unix system
FIND_LIBRARY(REMI_LIBRARY remi
			${REMI_INCLUDE_DIR}/../lib
			/usr/local/lib
			/usr/lib)

IF(REMI_INCLUDE_DIR)
	IF(REMI_LIBRARY)
		MESSAGE(STATUS "Remi found")
		SET(REMI_FOUND "YES")
		SET(REMI_LIBRARIES ${REMI_LIBRARY} ${CMAKE_DL_LIBS})
	ENDIF(REMI_LIBRARY)
ENDIF(REMI_INCLUDE_DIR)
