#!/bin/bash

if [[ "$(basename -- "$0")" == "script.sh" ]]; then
    echo "Don't run $0, source it" >&2
	exit 1
fi

spack load -r sdskeyval
spack load yaml-cpp
spack load mpich@3.2.1
spack load pufferscale

CONFS="LDT LT DT"
BUILD=.
NODES=$1


for CONF in $CONFS
do
	RESULT_FOLDER=$BUILD/results/results-$CONF
	CONF_FILE=../configs/config-$CONF.yaml
	mkdir -p $RESULT_FOLDER
	cp $CONF_FILE $RESULT_FOLDER/
	rm hepnos* log*
	(
		(
			mpirun -f $NODES $BUILD/src/HEPnOS-Pufferscale $CONF_FILE;
			pkill HEPnOS
		) &
		(
			sleep 30s;
			mpirun -n 256 -f $NODES $BUILD/src/HEPnOS-DataGenerator $CONF_FILE;
			$BUILD/src/HEPnOS-Controller $CONF_FILE `head -n 1 addresses.txt` 2>&1 | tee $RESULT_FOLDER/output.txt;
			pkill HEPnOS
		) 
	)
	cp hepnos* $RESULT_FOLDER/
done

