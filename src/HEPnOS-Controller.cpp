#include <iostream>
#include <iomanip>
#include <string>
#include <vector>
#include <memory>
#include <thallium.hpp>
#include <yaml-cpp/yaml.h>
#include <pufferscale/Controller.hpp>

#include <assert.h>

namespace tl = thallium;
namespace ps = pufferscale;

static std::string g_cfg_protocol                 = "ofi+tcp";
static uint16_t    g_cfg_pufferscale_master_id    = 1;

static int g_cfg_nb_sequences 						= 10;
static int g_cfg_nb_random_in_between				= 3;
static int g_cfg_warm_up_operations					= 10;
static int g_cfg_seed								= 0;
static int g_cfg_min_workers						= 1;
static int g_cfg_max_workers						= 2;
static std::vector<int> g_cfg_operation_initial_size;
static std::vector<int> g_cfg_operation_delta_nodes;


static std::unique_ptr<ps::Controller> g_controller;
static std::vector<ps::WorkerInfo>     g_commissioned_workers;
static std::vector<ps::WorkerInfo>     g_decommissioned_workers;

static void read_yaml_configuration(const std::string& configfile);
static void run_operations();

int main(int argc, char** argv) {
    if(argc != 3) {
        std::cout << "Usage: " << argv[0] << " <config.yaml> <Pufferscale_master_addr>" << std::endl;
    	exit(-1);
	}

    read_yaml_configuration(argv[1]);
    
	tl::engine engine(g_cfg_protocol, THALLIUM_CLIENT_MODE, false, 0);
	
	ps::MasterInfo masterinfo;
	masterinfo.m_address = std::string(argv[2]);
	masterinfo.m_provider_id = g_cfg_pufferscale_master_id;

	g_controller = std::make_unique<ps::Controller>(engine, masterinfo);

	// Work
	run_operations();

	g_controller.reset(); 

	return 0;
}

static void record_stats(int type, int before, int delta, int operation_number, double duration){
	double max_load = 0;
	double max_data = 0;
	double total_load = 0;
	double total_data = 0;
	
	std::unordered_map<ps::WorkerInfo,std::vector<ps::BucketInfo>,
			ps::WorkerInfoHash> all_buckets = g_controller->list_all_buckets();

	int counter = 0;

	for (const auto& elem : all_buckets){
		double local_load = 0;
		double local_data = 0;
		for (const ps::BucketInfo& ri : elem.second){
			counter++;
			local_load += ri.m_bucket_metadata.m_bucket_load;
			local_data += ri.m_bucket_metadata.m_bucket_data_in_memory;
		}
		total_load += local_load;
		total_data += local_data;
		max_load = std::max(max_load,local_load);
		max_data = std::max(max_data,local_data);
	}

	if (operation_number == 0){
		std::cout << "op,type,seed,current_nb_nodes,delta,max_load,total_load,max_data,";
		std::cout << "total_data,duration,buckets" << std::endl;
	}

	std::cout << std::setprecision(15);
	std::cout << operation_number << "," << type << "," << g_cfg_seed << "," << before << ",";
	std::cout << delta << "," << max_load << "," << total_load << "," << max_data << ",";
	std::cout << total_data << "," << duration << "," << counter << std::endl;

}

static void rescaling(int delta, int type, int& operation_number){

		int nb_workers_before = g_commissioned_workers.size();;

		// Record duration
		auto before = std::chrono::system_clock::now();

		if (delta > 0){
				// Commission
				std::unordered_map<ps::WorkerInfo, 
						std::vector<std::string>, 
						ps::WorkerInfoHash> to_com;
				for (int i = 0; i < delta; ++i){
						int pos = rand()%g_decommissioned_workers.size();
						// TODO commission the proper number of providers per node
						to_com[g_decommissioned_workers[pos]].push_back("sdskv");
						g_commissioned_workers.push_back(g_decommissioned_workers[pos]);
						g_decommissioned_workers.erase(g_decommissioned_workers.begin()+pos);
				}

				bool result = g_controller->commission(to_com);
				(void)result;
				if (!result){
						std::cout << "Operation (" << type << "): " << nb_workers_before << " + " << delta << std::endl;
						std::cout << "Failed commission" << std::endl;
				}

				assert(result);
		} else if (delta < 0) {
				// Decommission
				std::vector<ps::WorkerInfo> to_decom;
				for (int i = 0; i < -delta; ++i){
						int pos = rand()%g_commissioned_workers.size();
						to_decom.push_back(g_commissioned_workers[pos]);
						g_commissioned_workers.erase(g_commissioned_workers.begin()+pos);
				}

				bool result = g_controller->decommission(to_decom);
				(void)result;
				if (!result){
						std::cout << "Operation (" << type << "): " << nb_workers_before << " + " << delta << std::endl;
						std::cout << "Failed decommission" << std::endl;
				}
				assert(result);

				g_decommissioned_workers.insert(g_decommissioned_workers.end(), 
								to_decom.begin(),
								to_decom.end());
		}


		auto after = std::chrono::system_clock::now();
		std::chrono::duration<double> diff = after-before;

		record_stats(type, nb_workers_before, delta, operation_number, diff.count());

		operation_number++;
}

/** 
 * Run a rescaling of a random number of nodes
 */
static void random_rescaling(int& nb_operation){
		int delta = 0;
		int tries = 0;
		while (delta == 0 && tries < 5){
			delta = (rand() % (g_cfg_max_workers - g_cfg_min_workers + 1)) + g_cfg_min_workers - g_commissioned_workers.size();
			tries++;
		}
		rescaling(delta,-1,nb_operation);
}

// To run on controller
static void run_operations(){
		srand(g_cfg_seed);

		int delta;

		// initialize vectors of workerinfo
		std::unordered_map<ps::WorkerInfo,std::vector<ps::ProviderInfo>,
				ps::WorkerInfoHash> all_buckets = g_controller->list_all_providers();
		std::unordered_map<ps::WorkerInfo,std::vector<ps::ProviderInfo>,
				ps::WorkerInfoHash>::iterator it; 

		for (it = all_buckets.begin(); it != all_buckets.end(); ++it){
				g_commissioned_workers.push_back(it->first);
		}

		// Initialize the number of workers as the number of workers known by pufferscale
		int operation_nb       = 0;

		// Warm up
		for (int i = 0; i < g_cfg_warm_up_operations - 1; ++i){
				random_rescaling(operation_nb);
		}

		// Rescaling to the proper number of nodes for the next rescaling operation
		delta = g_cfg_operation_initial_size[0] - g_commissioned_workers.size();
		rescaling(delta,-1,operation_nb);

		for (int j = 0; j < g_cfg_nb_sequences ; ++j){
				for (unsigned int i = 0; i < g_cfg_operation_initial_size.size(); ++i){
						// Run the i-th rescaling operation
						rescaling(g_cfg_operation_delta_nodes[i],i,operation_nb);
						if (i == g_cfg_operation_initial_size.size() - 1 && j == g_cfg_nb_sequences - 1)
							break;

						for (int k = 0; k < g_cfg_nb_random_in_between - 1; ++k){
							random_rescaling(operation_nb);
						}

						// Rescaling to the proper number of nodes for the next rescaling operation
						delta = g_cfg_operation_initial_size[(i+1)%g_cfg_operation_initial_size.size()] - g_commissioned_workers.size();
						rescaling(delta,-1,operation_nb);
				}
		}
}



#define CFG_READ(__var, __section, __key) do {\
    if(YAML::Node parameter = config[__section][__key]) {\
        __var = parameter.as<decltype(__var)>();\
    }\
} while(0)

static void read_yaml_configuration(const std::string& configfile)
{
    YAML::Node config;
    try {
        config = YAML::LoadFile(configfile);
    } catch (YAML::BadFile& e) {
        std::cerr << "Couldnot read config file" << std::endl;
        exit(-2);
    }
    if (!config.IsMap()){
        std::cerr << "Invalid config file" << std::endl;
        exit(-3);
    }
    CFG_READ(g_cfg_protocol,                "thallium",     "protocol");
    CFG_READ(g_cfg_pufferscale_master_id,   "pufferscale",  "master_provider_id");
	CFG_READ(g_cfg_nb_sequences,            "controller",   "sequences");
	CFG_READ(g_cfg_nb_random_in_between,    "controller",   "in_between");
	CFG_READ(g_cfg_warm_up_operations,      "controller",   "warm_up");
	CFG_READ(g_cfg_seed,          			"controller",   "seed");
	CFG_READ(g_cfg_min_workers,        		"controller",   "min_workers");
	CFG_READ(g_cfg_max_workers,        		"controller",   "max_workers");

	for (unsigned int i = 0; i < config["controller"]["operation_initial_size"].size(); ++i){
		g_cfg_operation_initial_size.push_back(config["controller"]["operation_initial_size"][i].as<int>());
	}
	for (unsigned int i = 0; i < config["controller"]["operation_delta_nodes"].size(); ++i){
		g_cfg_operation_delta_nodes.push_back(config["controller"]["operation_delta_nodes"][i].as<int>());
	}

}

