#include <sstream>
#include <fstream>
#include <iomanip>
#include <map>
#include <mpi.h>
#include <margo.h>
#include <sdskv-client.h>
#include <thallium.hpp>
#include <yaml-cpp/yaml.h>
#include <random>
#include <math.h>
#include <assert.h>

namespace tl = thallium;

struct sdskv_provider_info {
    std::string                      address;
    uint16_t                         provider_id;
    std::vector<sdskv_database_id_t> databases;
    sdskv_provider_handle_t          provider_handle;
};

static std::string g_cfg_protocol                 = "ofi+tcp";
static int         g_cfg_sdskv_providers_per_node = 1;
static int         g_cfg_sdskv_db_per_provider    = 4;
static std::string g_cfg_sdskv_db_name_prefix     = "hepnos";
static bool        g_cfg_master_is_worker         = true;
static std::string g_cfg_addrfile                 = "addresses.txt";

// Config data generation
static double 	   g_cfg_rng_mean				  = 100;
static double 	   g_cfg_rng_std_dev			  = 40;
static int		   g_cfg_rng_seed				  = 0;
static int 		   g_cfg_rng_key_size			  = 32;
static int 		   g_cfg_rng_value_size			  = 64;

static sdskv_client_t g_sdskv_client = SDSKV_CLIENT_NULL;
static std::vector<sdskv_provider_info> g_sdskv_providers;

static void read_yaml_configuration(const std::string& configfile);
static void read_server_addresses_and_populate_sdskv_info(tl::engine& engine);
static void generate_data();

int main(int argc, char** argv) {
    MPI_Init(&argc, &argv);
    int rank, size, ret;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

    if(argc != 2) {
        if(rank == 0) {
            std::cout << "Usage: mpirun ... " << argv[0] << " <config.yaml>" << std::endl;
            MPI_Abort(MPI_COMM_WORLD, 1);
        }
    }

    read_yaml_configuration(argv[1]);

    tl::engine engine(g_cfg_protocol, THALLIUM_CLIENT_MODE, false, 0);

    ret = sdskv_client_init(engine.get_margo_instance(), &g_sdskv_client);

    read_server_addresses_and_populate_sdskv_info(engine);

    generate_data();

    for(auto& pinfo : g_sdskv_providers) {
        sdskv_provider_handle_release(pinfo.provider_handle);
    }

    ret = sdskv_client_finalize(g_sdskv_client);

    MPI_Finalize();

    (void)ret;
}

static std::string gen_random_string(size_t len) {
		static const char alphanum[] =
				"0123456789"
				"ABCDEFGHIJKLMNOPQRSTUVWXYZ"
				"abcdefghijklmnopqrstuvwxyz";
		std::string s(len, ' ');
		for (unsigned i = 0; i < len; ++i) {
				s[i] = alphanum[rand() % (sizeof(alphanum) - 1)];
		}
		return s;
}

static void generate_data()
{
	int rank, size, ret;
	MPI_Comm_rank(MPI_COMM_WORLD, &rank);
	MPI_Comm_size(MPI_COMM_WORLD, &size);

	sdskv_provider_handle_t provider;
	std::default_random_engine generator;
	std::normal_distribution<double> distribution;

	// Init RNG
	generator = std::default_random_engine(g_cfg_rng_seed+rank);
	distribution = std::normal_distribution<double>(g_cfg_rng_mean,g_cfg_rng_std_dev);


	// this function should loop over a number of keys, generating them
	// using sdskv_put. Note that since this is an MPI program, we can have
	// each rank take care of filling a subset of the providers.

	int grp_size = 1000;
	
	std::vector<std::string> keys(grp_size);
	std::vector<hg_size_t> keys_size(grp_size);
	std::vector<std::string> values(grp_size);
	std::vector<hg_size_t> values_size(grp_size);
	std::vector<const void*> keys_ptr(grp_size);
	std::vector<const void*> values_ptr(grp_size);

	for (int i = 0;i < grp_size; ++i){
		values[i] = gen_random_string(g_cfg_rng_value_size);
		// + 1 because of null char
		values_size[i] = g_cfg_rng_value_size+1;
		keys_size[i] = g_cfg_rng_key_size+1;
	}

	
	// All info about databases are in g_sdskv_providers
	int nb_db_to_populate = ceil((g_sdskv_providers.size()*g_cfg_sdskv_db_per_provider)/size);
	std::string tmp_str;
	for (int j = 0; j < nb_db_to_populate; ++j){
		
		int db_index = rank*nb_db_to_populate + j;
		int provider_to_populate = db_index/g_cfg_sdskv_db_per_provider;
		int db_to_populate = db_index%g_cfg_sdskv_db_per_provider;

		provider = g_sdskv_providers[provider_to_populate].provider_handle;

		const sdskv_database_id_t& db_id = g_sdskv_providers[provider_to_populate].databases[db_to_populate];
		for (int k = 0; k < grp_size; ++k){
				keys[k] = "";
				keys[k].resize(g_cfg_rng_key_size,'.');
		}
		// For one db
		int values_to_insert = (int)distribution(generator);
		if (g_cfg_rng_key_size <= log(values_to_insert)){
				std::cout << "g_cfg_rng_key_size (" << g_cfg_rng_key_size << 
						")too low for the number of keys to generate (" << values_to_insert << ")" << std::endl;
				MPI_Abort(MPI_COMM_WORLD,-10);
		}
		int i = 0;
		while (i < values_to_insert){
				int to_add = grp_size;
				if (i + to_add >= values_to_insert){
						to_add = values_to_insert - i;
				}
				for (int k = 0; k < to_add; ++k){
						tmp_str = std::to_string(i);
						keys[k].replace(0,tmp_str.length(),tmp_str);
						i++;
				}


				for (int k = 0; k < to_add; ++k){
						keys_ptr[k] = (const void*)keys[k].data();
						values_ptr[k] = (const void*)values[k].data();
				}

				ret = sdskv_put_multi(provider, 
								db_id, 
								to_add,  
								&keys_ptr[0], 
								keys_size.data(), 
								&values_ptr[0], 
								values_size.data());
				(void)ret;
				// TODO manage errors
				if (ret != 0){
						std::cout << "Failed insertion " << ret << " addr:" << g_sdskv_providers[provider_to_populate].address << " - DB: " << db_id << std::endl;
						std::cout << provider << std::endl;
						MPI_Abort(MPI_COMM_WORLD,-11);
				}
				//*/
		}
		std::cout << "Database populated." << std::endl;
	}
	std::cout << "Generation by rank " << rank << " terminated." << std::endl;
}

static void read_server_addresses_and_populate_sdskv_info(tl::engine& engine)
{
    std::ifstream infile(g_cfg_addrfile);
    uint64_t db_index = 0;
    bool skip_first_line = !g_cfg_master_is_worker;
    for(std::string addr; getline(infile, addr); )
    {
        if(skip_first_line) {
            skip_first_line = false;
            continue;
        }
        for(int i=0; i < g_cfg_sdskv_providers_per_node; i++) {
            sdskv_provider_info pinfo;
            auto endpoint = engine.lookup(addr);
            pinfo.provider_id = i+1;
			pinfo.address = addr;
            sdskv_provider_handle_create(g_sdskv_client, endpoint.get_addr(), i+1, &(pinfo.provider_handle));
            for(int j=0; j < g_cfg_sdskv_db_per_provider; j++) {
                sdskv_database_id_t db_id;
                std::stringstream ss;
                ss << g_cfg_sdskv_db_name_prefix << "."
                   << std::setw(6) << std::setfill('0')
                   << db_index;
				int ret = sdskv_open(pinfo.provider_handle, ss.str().c_str(), &db_id);
                if (ret != 0){
					MPI_Abort(MPI_COMM_WORLD,-12);
				}
				
				pinfo.databases.push_back(db_id);
                db_index += 1;
            }
            g_sdskv_providers.push_back(pinfo);
        }
    }
}

#define CFG_READ(__var, __section, __key) do {\
    if(YAML::Node parameter = config[__section][__key]) {\
        __var = parameter.as<decltype(__var)>();\
    }\
} while(0)

static void read_yaml_configuration(const std::string& configfile)
{
    YAML::Node config;
    try {
        config = YAML::LoadFile(configfile);
    } catch (YAML::BadFile& e) {
        std::cerr << "Couldnot read config file" << std::endl;
        exit(-2);
    }
    if (!config.IsMap()){
        std::cerr << "Invalid config file" << std::endl;
        exit(-3);
    }
    CFG_READ(g_cfg_protocol,                "thallium",     "protocol");
    CFG_READ(g_cfg_sdskv_providers_per_node,"sdskv",        "providers_per_node");
    CFG_READ(g_cfg_sdskv_db_per_provider,   "sdskv",        "databases_per_provider");
    CFG_READ(g_cfg_sdskv_db_name_prefix,    "sdskv",        "databases_name_prefix");
    CFG_READ(g_cfg_master_is_worker,        "pufferscale",  "master_is_worker");
    CFG_READ(g_cfg_addrfile,                "connections",  "address_file");
	CFG_READ(g_cfg_rng_mean,                "generation",   "mean");
	CFG_READ(g_cfg_rng_std_dev,             "generation",   "std_dev");
	CFG_READ(g_cfg_rng_seed,                "generation",   "seed");
	CFG_READ(g_cfg_rng_key_size,            "generation",   "key_size");
	CFG_READ(g_cfg_rng_value_size,          "generation",   "value_size");
}
