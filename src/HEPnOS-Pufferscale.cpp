#include <sstream>
#include <iomanip>
#include <map>
#include <mpi.h>
#include <margo.h>
#include <sdskv-server.h>
#include <sdskv-client.h>
#include <thallium.hpp>
#include <yaml-cpp/yaml.h>
#include <pufferscale/Worker.hpp>
#include <pufferscale/Master.hpp>
#include "Logging.hpp"
#include <random>
#include <abt-io.h>
#include <mutex>

namespace tl = thallium;
namespace ps = pufferscale;

static std::string g_cfg_protocol                 = "ofi+tcp";
static bool        g_cfg_use_progress_thread      = true;
static int         g_cfg_num_rpc_xstreams         = 0;
static int         g_cfg_num_abtio_threads        = 1;
static int         g_cfg_sdskv_providers_per_node = 1;
static std::string g_cfg_sdskv_database_type      = "leveldb";
static int         g_cfg_sdskv_db_per_provider    = 4;
static std::string g_cfg_sdskv_db_path            = "/dev/shm";
static std::string g_cfg_sdskv_db_name_prefix     = "hepnos";
static uint16_t    g_cfg_pufferscale_worker_id    = 1;
static uint16_t    g_cfg_pufferscale_master_id    = 1;
static double      g_cfg_memory_capacity          = 1.0;
static double      g_cfg_network_bandwidth        = 1.0;
static double      g_cfg_disk_capacity            = 1.0;
static double      g_cfg_disk_read_bw             = 1.0;
static double      g_cfg_disk_write_bw            = 1.0;
static double      g_cfg_weight_load              = 1.0;
static double      g_cfg_weight_data              = 1.0;
static double      g_cfg_weight_transfers         = 1.0;
static bool        g_cfg_use_disk                 = false;
static bool        g_cfg_use_memory               = true;
static int         g_cfg_parallel_migrations      = 1;
static int         g_cfg_parallel_transfers       = 1;
static bool        g_cfg_master_is_worker         = true;
static std::string g_cfg_logfile                  = "log";
static std::string g_cfg_addrfile                 = "addresses.txt";

static double      g_cfg_rng_load_total           = 100;
static double      g_cfg_rng_mean                 = 100;
static double      g_cfg_rng_std_dev              = 40;
static int         g_cfg_rng_seed                 = 0;
static bool        g_active						  = true;

static LogStream                g_logger;
static std::vector<std::string> g_addresses;
static uint64_t                 g_total_num_databases;
static int                      g_worker_size; // total number of MPI ranks acting as workers
static int                      g_worker_rank; // id of this process as worker or negative if not a worker
static sdskv_client_t           g_sdskv_client; // client to use to issue migration orders to SDSKV
static std::map<int16_t, sdskv_provider_handle_t> g_sdskv_local_provider_handles; // provider handle to local providers
static std::map<int16_t, ps::ProviderInfo>        g_inactive_sdskv_providers; // set of inactive SDSKV providers in this node
static std::map<int16_t, ps::ProviderInfo>        g_sdskv_providers; // set of SDSKV providers in this node
static std::map<int16_t, sdskv_provider_t>        g_sdskv_local_providers; // local providers
static std::map<uint64_t, std::pair<sdskv_provider_t, sdskv_database_id_t>> g_sdskv_local_databases; // mapping from BucketID to a sdskv_provider_t and a sdskv_database_id_t
static std::unique_ptr<tl::mutex>	g_sdskv_mutex_local_databases;
static std::unique_ptr<ps::Worker> 	g_worker;
static std::vector<double>    		g_databases_loads;

//static abt_io_instance_id			g_abtio_id;

static void share_mercury_addresses(MPI_Comm comm, tl::engine& engine);
static void initialize_sdskv_entities(tl::engine& engine);
static void initialize_pufferscale_entities(MPI_Comm comm, tl::engine& engine);
static void read_yaml_configuration(const std::string& configfile);
static void write_addresses_to_file();
static void post_migration_callback(sdskv_provider_t provider, const sdskv_config_t* config, sdskv_database_id_t db_id, void* uargs); 



class Xfer_scheduler : public tl::provider<Xfer_scheduler> {
	private:
		tl::remote_procedure m_xfer_wait;
		tl::remote_procedure m_xfer_will_start;
		tl::remote_procedure m_xfer_end;

		tl::engine& m_engine;
		std::unique_ptr<tl::mutex> m_recv_mutex;
		int m_xfer_concurrent_recv = 0;

		std::unique_ptr<tl::mutex> m_send_mutex;
		int m_xfer_concurrent_send = 0;

		std::unique_ptr<tl::mutex> m_cv_send_mutex;	
		tl::condition_variable m_cv_send;
		std::unique_ptr<tl::mutex> m_cv_recv_mutex;	
		tl::condition_variable m_cv_recv;

		// Wait until 
		bool xfer_wait(){
			std::unique_lock<tl::mutex> lck(*m_cv_recv_mutex);
			
			m_recv_mutex->lock();
			while (m_xfer_concurrent_recv > g_cfg_parallel_transfers){
				m_recv_mutex->unlock();
				// Wait till there are not too many transfers out
				m_cv_recv.wait(lck);	
				m_recv_mutex->lock();
			}	
			m_xfer_concurrent_recv++;
			m_recv_mutex->unlock();
			
			return true;
		}

		bool xfer_start(bool b){
			if (!b){
				m_recv_mutex->lock();
				m_xfer_concurrent_recv--;
				m_recv_mutex->unlock();
				m_cv_recv.notify_one();
			}
			return true;
		}

		// Decrease th counter of concurrent receptions
		bool xfer_end(){
			m_recv_mutex->lock();
			m_xfer_concurrent_recv--;
			m_recv_mutex->unlock();
			m_cv_recv.notify_one();
			return true;
		}

	public:
		Xfer_scheduler(tl::engine& engine):
			tl::provider<Xfer_scheduler>(engine,1),
			m_xfer_wait(engine.define("xfer_wait")),
			m_xfer_will_start(engine.define("xfer_start")),
			m_xfer_end(engine.define("xfer_end")),
			m_engine(engine){
			define("xfer_wait",&Xfer_scheduler::xfer_wait);	
			define("xfer_start",&Xfer_scheduler::xfer_start);	
			define("xfer_end",&Xfer_scheduler::xfer_end);	
			m_recv_mutex = std::make_unique<tl::mutex>();
			m_send_mutex = std::make_unique<tl::mutex>();
			m_cv_recv_mutex = std::make_unique<tl::mutex>();
			m_cv_send_mutex = std::make_unique<tl::mutex>();
		}

		~Xfer_scheduler(){
			m_recv_mutex.reset();
			m_send_mutex.reset();
			m_cv_recv_mutex.reset();
			m_cv_send_mutex.reset();
		}

		void wait_until_xfer(std::string addr){
            auto endpoint = m_engine.lookup(addr);
			auto h = tl::provider_handle(endpoint,1);
			
			std::unique_lock<tl::mutex> lck(*m_cv_send_mutex);
		
			while (true){
				m_send_mutex->lock();
				while (m_xfer_concurrent_send > g_cfg_parallel_transfers){
					m_send_mutex->unlock();
					// Wait till there are not too many transfers out
					m_cv_send.wait(lck);	
					m_send_mutex->lock();
				}
				m_send_mutex->unlock();

				// Wait in a rpc will return when the receiver can receive
				bool ret = m_xfer_wait.on(h)();
				(void) ret;

				// Check whether we can send
				m_send_mutex->lock();
				if (m_xfer_concurrent_send >= g_cfg_parallel_transfers){
					// Transfer can't start until other transfers terminate
					m_send_mutex->unlock();
					ret = m_xfer_will_start.on(h)(false);
					(void) ret;
				} else {
					// Transfer can start
					m_xfer_concurrent_send++;
					m_send_mutex->unlock();	
					ret = m_xfer_will_start.on(h)(true);
					(void) ret;
					break;
				} 
			}
		}
		
		void xfer_terminated(std::string addr){
            auto endpoint = m_engine.lookup(addr);
			auto h = tl::provider_handle(endpoint,1);
			
			bool ret = m_xfer_end.on(h)();
			(void)ret;
			m_send_mutex->lock();
			m_xfer_concurrent_send--;
			m_send_mutex->unlock();
			m_cv_send.notify_one();
		}
};


std::unique_ptr<Xfer_scheduler> g_xfer_scheduler;


int main(int argc, char** argv)
{
    MPI_Init(&argc, &argv);
    int rank, size;
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    MPI_Comm_size(MPI_COMM_WORLD, &size);

	/*
	for (int i = 0; i < size; ++i){
		if (rank == i){
			std::cout << rank << std::endl;
			system("ulimit -s");
		}
		MPI_Barrier(MPI_COMM_WORLD);
	}
	//*/
    if(argc != 2) {
        if(rank == 0) {
            std::cout << "Usage: mpirun ... " << argv[0] << " <config.yaml>" << std::endl;
            MPI_Abort(MPI_COMM_WORLD, 1);
        }
    }
    MPI_Barrier(MPI_COMM_WORLD);

    // Read the configuration. This will set the g_cfg_* variables declared above.
    read_yaml_configuration(argv[1]);

    // Initialize engine.
    tl::engine engine(g_cfg_protocol, THALLIUM_SERVER_MODE, 
            g_cfg_use_progress_thread, g_cfg_num_rpc_xstreams);
    engine.enable_remote_shutdown();

	// Initialize ABT_IO
	/*
	g_abtio_id = abt_io_init(g_cfg_num_abtio_threads);
	if (g_abtio_id == NULL){
		g_logger.error() << "ABTIO NULL" << std::endl;
		MPI_Abort(MPI_COMM_WORLD, -40);
	}
	//*/

	g_sdskv_mutex_local_databases = std::make_unique<tl::mutex>();

	g_xfer_scheduler = std::make_unique<Xfer_scheduler>(engine);

	engine.push_finalize_callback([](){//abt_io_finalize(g_abtio_id);
							g_sdskv_mutex_local_databases.reset();
							g_xfer_scheduler.reset();});	

    // Initialize logging. Each server will use its own log file. The name prefix
    // can be set in the YAML configuration file.
    std::stringstream ss;
    ss << g_cfg_logfile << "." << std::setw(6) << std::setfill('0') << rank;
    g_logger = LogStream(ss.str());

    // Share mercury addresses
    share_mercury_addresses(MPI_COMM_WORLD, engine);
    
	for (const std::string& addr : g_addresses){
		engine.lookup(addr);
	}
	
	
	// Initialize some global variables. Note that g_worker_rank should not be used with a communicator
    // since this is not an actual MPI rank, just a worker rank to help compute database ids at the 
    // beginning.
    g_worker_size = g_cfg_master_is_worker ? size : size-1;
    g_worker_rank = g_cfg_master_is_worker ? rank : rank-1;
    g_total_num_databases = g_worker_size*g_cfg_sdskv_providers_per_node*g_cfg_sdskv_db_per_provider;

    // Writing addresses to a file specified in the YAML file. This file will be read by client programs
    // to interact with the SDSKV providers.
    if(rank == 0)
        write_addresses_to_file();

    // Initialize SDSKV providers and clients.
    initialize_sdskv_entities(engine);

    // Initialize Pufferscale master and workers.
    initialize_pufferscale_entities(MPI_COMM_WORLD, engine);

    // Wait for someone to shutdown this server
    g_logger.log() << "Starting main loop" << std::endl;

	if (rank == 0)
		std::cout << "Starting main loop" << std::endl;

	engine.wait_for_finalize();

    MPI_Finalize();
    return 0;
}

static void initialize_sdskv_entities(tl::engine& engine)
{
    g_logger.log() << "Initializing SDSKV entities" << std::endl;
    if(g_worker_rank < 0){
        return;
	}

	int rank = g_worker_rank + (g_cfg_master_is_worker?0:1);

    // SDSKV's API is in C so we need the underlying margo instance.
    auto mid = engine.get_margo_instance();
    uint32_t ret = SDSKV_SUCCESS;
    // Initialize the SDSKV client (needed to issue migrations).
    ret = sdskv_client_init(engine.get_margo_instance(), &g_sdskv_client);
    if(ret != SDSKV_SUCCESS) {
        g_logger.error() << "sdskv_client_init failed with error code " << ret << std::endl;
        exit(-1);
    }

    // Ger the address of this process, as a hg_addr_t.
    auto this_endpoint = engine.self();
    hg_addr_t this_addr = this_endpoint.get_addr();
    if(this_addr == HG_ADDR_NULL) {
      g_logger.error() << "Could not get address of this process as hg_addr_t handle" << std::endl;
        exit(-1);
    }

    // Initialize providers and provider handles.
    for(int i = 0; i < g_cfg_sdskv_providers_per_node; i++) {
        sdskv_provider_t sdskv_prov;
        g_logger.log() << "Registering SDSKV provider" << std::endl;
        ret = sdskv_provider_register(mid, i+1, SDSKV_ABT_POOL_DEFAULT, &sdskv_prov);
        if(ret != SDSKV_SUCCESS) {
            g_logger.error() << "Could not register SDSKV provider" << std::endl;
            exit(-1);
        }
        g_sdskv_local_providers[i+1] = sdskv_prov;

        // Set the ABT-IO instance to be used
		/*
		ret = sdskv_provider_set_abtio_instance(sdskv_prov, g_abtio_id);
        if(ret != SDSKV_SUCCESS) {
            g_logger.error() << "Could not set ABTIO" << std::endl;
            exit(-1);
        }
		//*/

		int* id = new int;
		*id = i + 1;

        sdskv_provider_set_migration_callbacks(sdskv_prov, nullptr, post_migration_callback, id);
        /// Database type can be configured in the YAML file.
        sdskv_db_type_t db_type = KVDB_LEVELDB;
        if(g_cfg_sdskv_database_type == "leveldb")    db_type = KVDB_LEVELDB;
        if(g_cfg_sdskv_database_type == "berkeleydb") db_type = KVDB_BERKELEYDB;
        // Initialize all the databases for this provider.
        for(int j = 0; j < g_cfg_sdskv_db_per_provider; j++) {
            sdskv_database_id_t db_id;
            sdskv_config_t config;
            std::memset(&config, 0, sizeof(config));
            std::stringstream ss;
            // database index
            uint64_t this_db_id = g_cfg_sdskv_db_per_provider*(g_worker_rank*g_cfg_sdskv_providers_per_node + i) + j;
            // database name
            ss << g_cfg_sdskv_db_name_prefix << "." 
               << std::setw(6) << std::setfill('0') 
               << this_db_id;
            std::string db_name = ss.str();
            config.db_name = db_name.c_str();
			std::stringstream ss2;
			ss2 << g_cfg_sdskv_db_path << "/provider-" << std::setw(6) << std::setfill('0') << rank;
            std::string db_path = ss2.str();
			config.db_path = db_path.c_str();
            config.db_type = db_type;
            g_logger.log() << "Attaching database " << db_name << std::endl;
            ret = sdskv_provider_attach_database(sdskv_prov, &config,  &db_id);
            if(ret != SDSKV_SUCCESS) {
                g_logger.error() << "Could not attach database to provider" << std::endl;
                exit(-1);
            }
            g_sdskv_local_databases[this_db_id] = std::make_pair(sdskv_prov,db_id);
        }
        // Creating the provider handle corresponding to the provider we just created.
        sdskv_provider_handle_t ph;
        ret = sdskv_provider_handle_create(g_sdskv_client, this_addr, i+1, &ph);
        if(ret != SDSKV_SUCCESS) {
            g_logger.error() << "Could not create SDSKV provider handle" << std::endl;
            exit(-1);
        }
        g_sdskv_local_provider_handles[i+1] = ph;
    }

    // When the engine finalizes, we want to destroy the provider handles and the SDSKV client.
    engine.push_finalize_callback([]() {
            for(auto& p : g_sdskv_local_provider_handles) {
                sdskv_provider_handle_release(p.second);
            }
            sdskv_client_finalize(g_sdskv_client);
        });

    g_logger.log() << "Done initializing SDSKV entities" << std::endl;
}

/* Right now we cannot deregister a provider easily with Margo, so what we
   are doing here is keep track of providers that are inactive, and reactivate
   them when the node is brought back up. */


/* callback called by the Worker to initiate a new SDSKV provider */
static ps::ProviderInitializationData initiate_sdskv_provider(void* uargs)
{
    (void)uargs;
    ps::ProviderInitializationData ret;
    if(g_inactive_sdskv_providers.empty()) {
        ret.m_success = false;
        return ret;
    }

    auto it = g_inactive_sdskv_providers.begin();
    ret.m_provider_info = it->second;
    g_inactive_sdskv_providers.erase(it);
   
	g_active = true;	

    return ret;
}

/* callback called by the Worker to terminate a running SDSKV provdier */
static bool terminate_sdskv_provider(const ps::ProviderInfo& pi, void* args1, void* args2)
{
    (void)args1;
    (void)args2;

    g_inactive_sdskv_providers[pi.m_provider_id] = pi;
	g_active = false;

    return true;
}

/* callback called by the Worker to issue a migration */
bool migration_callback(const ps::MigrationOperation ops, void* uargs)
{

	int rank = g_worker_rank + (g_cfg_master_is_worker?0:1);

    int ret = SDSKV_SUCCESS;
    (void)uargs;
    // identify the bucket to migrate
    const auto& bucket_id      = ops.m_bucket.m_bucket_id;
    const auto& source_prov_id   = ops.m_source.m_provider_id;
    const auto& dest_prov_id     = ops.m_destination.m_provider_id;
    const auto& dest_prov_addr   = ops.m_destination.m_provider_address;

	if (dest_prov_addr == g_addresses[rank]){
		g_logger.error() << "Sending data to itself [" << bucket_id << "]" 
				<< "(dst: " << dest_prov_addr << " / src:" << ops.m_source.m_provider_address << " / " 
				<< g_addresses[rank]  << ")" << std::endl;
		return true;
	}

    if(g_sdskv_local_provider_handles.count(source_prov_id) == 0) {
        g_logger.error() << "Could not find provider id " << source_prov_id 
            << " when issuing migration" << std::endl;
        return false;
    }
	{
			std::lock_guard<tl::mutex> guard(*g_sdskv_mutex_local_databases);
			if(g_sdskv_local_databases.count(bucket_id) == 0) {
					g_logger.error() << "Trying to migrate database with bucket id "
							<< bucket_id << " but it does not appear to be on this node" << std::endl;
					return false;
			}
	}

	g_xfer_scheduler->wait_until_xfer(dest_prov_addr);

	// get the right provider handle for the source provider
    sdskv_provider_handle_t source_ph = g_sdskv_local_provider_handles[source_prov_id];
    // compute the name of the database to migrate
    // lookup the database
    sdskv_database_id_t source_db_id;
    std::stringstream ss;
    ss << g_cfg_sdskv_db_name_prefix << "." 
       << std::setw(6) << std::setfill('0') 
       << bucket_id;
    std::string db_name = ss.str();
    ret = sdskv_open(source_ph, db_name.c_str(), &source_db_id);
    if(ret != SDSKV_SUCCESS) {
        g_logger.error() << "Could not open database " << db_name
            << " for migration (return code is " << ret << ")" << std::endl;
        return false;
    }
   
	unsigned int index_addr = 0;
	while (index_addr < g_addresses.size()){
		if (g_addresses[index_addr] == dest_prov_addr){
			break;
		}
		index_addr++;
	}
	g_logger.lock();
	g_logger.log() << "SEND: RANK " << rank << " -[" << bucket_id << "]-> " << index_addr 
				<< "(dst: " << dest_prov_addr << " / src:" << g_addresses[rank] << ")" << std::endl;
	g_logger.unlock();
	// migrate the database
    std::stringstream ss2;
	ss2 << g_cfg_sdskv_db_path << "/provider-" << std::setw(6) << std::setfill('0') << index_addr;
    std::string db_path = ss2.str();
	ret = sdskv_migrate_database(source_ph, source_db_id,
            dest_prov_addr.c_str(), dest_prov_id,
            db_path.c_str(), SDSKV_REMOVE_ORIGINAL);
	g_xfer_scheduler->xfer_terminated(dest_prov_addr);
    if(ret != SDSKV_SUCCESS) { 
		g_logger.lock();
		g_logger.error() << "Could not migrate database " << db_name
            << " (return code is " << ret << ", remi_errno: " << sdskv_remi_errno << " )" << std::endl;
        g_logger.unlock();
		sdskv_remi_errno = 0;
		return false;
    }

	// remove the database from the databases known to this node
    {
		std::lock_guard<tl::mutex> guard(*g_sdskv_mutex_local_databases);
		g_sdskv_local_databases.erase(bucket_id);
	}
    return true;
}

/* callback called by the Worker to issue a metadata update */
ps::BucketMetadata update_meta_callback(const ps::BucketTag& rt, void* uargs)
{
	std::lock_guard<tl::mutex> guard(*g_sdskv_mutex_local_databases);
    (void)uargs;
    ps::BucketMetadata result = {0.0, 0.0, 0.0};
    auto id = rt.m_bucket_id;
    auto it = g_sdskv_local_databases.find(id);
    if(it == g_sdskv_local_databases.end()) {
        g_logger.error() << "In update_metadata_callback, bucket with id "
            << id << " not found on this server" << std::endl;
        return result;
    }
    sdskv_provider_t provider = it->second.first;
    sdskv_database_id_t db_id = it->second.second;
    size_t size = 0;
    int ret = sdskv_provider_compute_database_size(provider, db_id, &size);
    if(ret != SDSKV_SUCCESS) {
        g_logger.error() << "Could not compute database size "
            << " (return code is " << ret << ", remi_errno: " << sdskv_remi_errno << " )" << std::endl;
        
		return result;
    }
	if (size == 0){
		g_logger.log() << "Object " << id << " of size 0." << std::endl;
	}
    result.m_bucket_data_on_disk = 0.0;
    result.m_bucket_data_in_memory = size;
    result.m_bucket_load = g_databases_loads[id]; 

	//g_logger.log() << "Bucket " << rt << " has load " << result.m_bucket_load << " and size " << size << std::endl;

    return result;
}

static void generate_loads(){
    std::default_random_engine generator;
    std::normal_distribution<double> distribution;

    // Init RNG
    generator = std::default_random_engine(g_cfg_rng_seed);
    distribution = std::normal_distribution<double>(g_cfg_rng_mean,g_cfg_rng_std_dev);

    int total_db = g_worker_size*g_cfg_sdskv_providers_per_node*g_cfg_sdskv_db_per_provider;
    g_databases_loads.resize(total_db);
    double total = 0;
    double load;
    for (int i = 0; i < total_db; ++i){
        load = distribution(generator);
        if (load < 1){
            load = 1;
        }
        g_databases_loads[i] = load;
        total += load;
    }
    
	double r = g_cfg_rng_load_total / total;
    for (int i = 0; i < total_db; ++i){
        g_databases_loads[i] *= r;
    }
}

static void pre_rescaling(void* uargs){
	(void)uargs;
	g_logger.log() << "Starting rescaling" << std::endl;
}

static void post_rescaling(void* uargs){
	(void)uargs;
	g_logger.log() << "Ending rescaling" << std::endl;
}

static void initialize_pufferscale_entities(MPI_Comm comm, tl::engine& engine)
{
    g_logger.log() << "Initializing Pufferscale providers" << std::endl;
    int rank;
    MPI_Comm_rank(comm, &rank);
    // Rank 0 becomes a Master
    if(rank == 0) {
        auto master = new ps::Master(engine, g_cfg_pufferscale_master_id);
        engine.push_finalize_callback([master]() { delete master; });
        master->configure_provider("sdskv",
                g_cfg_weight_load,
                g_cfg_weight_data,
                g_cfg_weight_transfers,
                g_cfg_use_disk,
                g_cfg_use_memory,
                g_cfg_parallel_migrations);
    }

    MPI_Barrier(comm);

    // If the YAML file has master_is_worker, the master will have g_worker_rank == 0,
    // otherwise it will have g_worker_rank == -1.
    if(g_worker_rank < 0)
        return;
    
    generate_loads();

    // Initialize information about the master
    ps::MasterInfo masterInfo = { g_addresses[0], g_cfg_pufferscale_master_id };

    // Initialize the worker
    g_worker = std::make_unique<ps::Worker>(engine, g_cfg_pufferscale_worker_id);
    engine.push_finalize_callback([]() { g_worker.reset(); });
    g_worker->configure_worker(g_cfg_memory_capacity, g_cfg_network_bandwidth);
    g_worker->configure_disk(g_cfg_disk_capacity, g_cfg_disk_read_bw, g_cfg_disk_write_bw);
    g_worker->register_provider("sdskv", nullptr,
                              initiate_sdskv_provider,
                              terminate_sdskv_provider,
							  pre_rescaling,
							  post_rescaling);
    // Add information about the SDSKV providers that are present on this node
    for(int i = 0; i < g_cfg_sdskv_providers_per_node; i++) {
        ps::ProviderInfo pi = { (uint16_t)(i+1), "sdskv", g_addresses[rank] };
        g_sdskv_providers[i+1] = pi;
		g_worker->manages_provider(pi);
        for(int j = 0; j < g_cfg_sdskv_db_per_provider; j++) {
            ps::BucketTag rt;
            rt.m_service_name = "sdskv";
            uint64_t id = g_cfg_sdskv_db_per_provider*(g_worker_rank*g_cfg_sdskv_providers_per_node + i) + j;
            rt.m_bucket_id = id;
            g_worker->manages(pi, rt, migration_callback, update_meta_callback, nullptr); 
        }
    }
    // Make the Worker join the Master
    g_worker->join(masterInfo);
    g_logger.log() << "Worker joined" << std::endl;
}

// Share the mercury addresses using MPI
static void share_mercury_addresses(MPI_Comm comm, tl::engine& engine)
{
    g_logger.log() << "Starting to share Mercury addresses" << std::endl;
    int rank, size;
    MPI_Comm_size(comm, &size);
    MPI_Comm_rank(comm, &rank);
    auto self_addr = static_cast<std::string>(engine.self());
    self_addr.resize(256);
    std::vector<char[256]> addresses(size);
    MPI_Allgather(self_addr.c_str(),
            256, MPI_BYTE, addresses.data(), 256, MPI_BYTE, comm);
    g_addresses.resize(size);
    for(int i = 0; i < size; i++) {
        g_addresses[i]  = addresses[i];
    }
    g_logger.log() << "Done sharing Mercury addresses" << std::endl;
}

#define CFG_READ(__var, __section, __key) do {\
        if(YAML::Node parameter = config[__section][__key]) {\
            __var = parameter.as<decltype(__var)>();\
        }\
    } while(0)

// Reads the YAML configuration file.
static void read_yaml_configuration(const std::string& configfile)
{
    g_logger.log() << "Reading configuration file " << std::endl;
    YAML::Node config;
    try {
        config = YAML::LoadFile(configfile);
    } catch (YAML::BadFile& e) {
        g_logger.error() << "Could not read config file" << std::endl;
        exit(-2);
    }
    if (!config.IsMap()){
        g_logger.error() << "Invalid config file" << std::endl;
        exit(-3);
    }
    CFG_READ(g_cfg_protocol,                "thallium",     "protocol");
    CFG_READ(g_cfg_use_progress_thread,     "thallium",     "use_progress_thread");
    CFG_READ(g_cfg_num_rpc_xstreams,        "thallium",     "num_rpc_xstreams");
    CFG_READ(g_cfg_num_abtio_threads,       "sdskv"   ,     "num_abtio_threads");
    CFG_READ(g_cfg_sdskv_providers_per_node,"sdskv",        "providers_per_node");
    CFG_READ(g_cfg_sdskv_database_type,     "sdskv",        "database_type");
    CFG_READ(g_cfg_sdskv_db_per_provider,   "sdskv",        "databases_per_provider");
    CFG_READ(g_cfg_sdskv_db_path,           "sdskv",        "databases_path");
    CFG_READ(g_cfg_sdskv_db_name_prefix,    "sdskv",        "databases_name_prefix");
    CFG_READ(g_cfg_pufferscale_worker_id,   "pufferscale",  "worker_provider_id");
    CFG_READ(g_cfg_pufferscale_master_id,   "pufferscale",  "master_provider_id");
    CFG_READ(g_cfg_weight_load,             "pufferscale",  "weight_load");
    CFG_READ(g_cfg_weight_data,             "pufferscale",  "weight_data");
    CFG_READ(g_cfg_weight_transfers,        "pufferscale",  "weight_transfers");
    CFG_READ(g_cfg_parallel_migrations,     "pufferscale",  "parallel_migrations");
    CFG_READ(g_cfg_parallel_transfers,      "pufferscale",  "parallel_transfers");
    CFG_READ(g_cfg_master_is_worker,        "pufferscale",  "master_is_worker");
    CFG_READ(g_cfg_memory_capacity,         "hardware",     "memory_capacity");
    CFG_READ(g_cfg_network_bandwidth,       "hardware",     "network_bandwidth");
    CFG_READ(g_cfg_disk_capacity,           "hardware",     "disk_capacity");
    CFG_READ(g_cfg_disk_read_bw,            "hardware",     "disk_read_bw");
    CFG_READ(g_cfg_disk_write_bw,            "hardware",     "disk_write_bw");
    CFG_READ(g_cfg_use_disk,                "hardware",     "use_disk");
    CFG_READ(g_cfg_use_memory,              "hardware",     "use_memory");
    CFG_READ(g_cfg_logfile,                 "logging",      "file_prefix");
    CFG_READ(g_cfg_addrfile,                "connections",  "address_file");
    CFG_READ(g_cfg_rng_load_total,          "load",          "total");
    CFG_READ(g_cfg_rng_mean,                "load",          "mean");
    CFG_READ(g_cfg_rng_std_dev,             "load",          "std_dev");
    CFG_READ(g_cfg_rng_seed,                "load",          "seed");
    g_logger.log() << "Done reading configuration file" << std::endl;
}

// Writes the address array to a file.
static void write_addresses_to_file()
{
    std::ofstream file(g_cfg_addrfile);
    for(auto& addr : g_addresses) {
        file << addr << std::endl;
    }
}

// callback registered to the SDSKV providers to be called when receiving a database
static void post_migration_callback(
        sdskv_provider_t provider,
        const sdskv_config_t* config,
        sdskv_database_id_t db_id,
        void* uargs)
{
    //(void)provider;
    // (void)uargs;
	int provider_id =*(int*)(uargs);

	int rank = g_worker_rank + (g_cfg_master_is_worker?0:1);

    // (1) compute the BucketID of the database from its name. Note: the name
    //  is in the form blablabla.0000X whith X being the database id we need to read.
    std::string db_name = config->db_name;
    auto point_position = db_name.find_last_of('.');
    if(point_position == std::string::npos) {
        g_logger.error() << "Could not parse BucketID from DataBase name (" << db_name << ")" << std::endl;
        exit(-1);
    }
    const char* id_str = config->db_name + point_position;
    while((id_str[0] == '.' || id_str[0] == '0') && id_str[0] != '\0') {
        id_str += 1;
    }
    ps::BucketID id = atol(id_str);
    // (2) add the database to the g_sdskv_local_databases variable
	{
		std::lock_guard<tl::mutex> guard(*g_sdskv_mutex_local_databases);
  	  	g_sdskv_local_databases[id] = std::make_pair(provider,db_id);
	}
	g_logger.lock();
	g_logger.log() << "RECV: RANK " << rank << " <-[" << id << "]-"<< std::endl;
	g_logger.unlock();
	// TODO Need to run worker->manages for the db to be managed by Pufferscale again
	ps::BucketTag rt;
	rt.m_service_name = "sdskv";
	rt.m_bucket_id = id;
	if (!g_active){
		g_logger.error() << "Received " << id << " while inactive" << std::endl;
		MPI_Abort(MPI_COMM_WORLD, -41);
	}
	
	if (g_sdskv_providers.count(provider_id) == 0){
		g_logger.error() << "Provider id invalid" << std::endl;
		MPI_Abort(MPI_COMM_WORLD,-43);
	}


	if (!g_worker->manages(g_sdskv_providers[provider_id], rt, migration_callback, update_meta_callback, nullptr)){
		g_logger.error() << "Manages failed" << std::endl;
		MPI_Abort(MPI_COMM_WORLD, -42);
	}
}
