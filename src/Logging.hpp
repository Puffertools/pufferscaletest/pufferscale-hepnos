#ifndef __LOGGING
#define __LOGGING

#include <fstream>
#include <iomanip>
#include <mpi.h>
#include <mutex>
#include <thallium.hpp>

namespace tl = thallium;

class LogStream {

    private:

        std::ofstream              m_stream;
        std::unique_ptr<tl::mutex> m_mutex;

    public:

        LogStream() = default;

        explicit LogStream(const std::string& filename)
        : m_stream(filename), m_mutex(std::make_unique<tl::mutex>()) {}

        LogStream(const LogStream&) = delete;

        LogStream(LogStream&&) = default;

        LogStream& operator=(const LogStream&) = delete;

        LogStream& operator=(LogStream&&) = default;

        ~LogStream() = default;

        std::ofstream& log() {
            m_stream << "[INFO " << std::fixed << std::showpoint << std::setprecision(9) << MPI_Wtime() << "] ";
            return m_stream;
        }

        std::ofstream& error() {
            m_stream << "[ERROR " << std::fixed << std::showpoint << std::setprecision(9) << MPI_Wtime() << "] ";
            return m_stream;
        }

        void lock() {
            if(m_mutex) m_mutex->lock();
        }

        void unlock() {
            if(m_mutex) m_mutex->unlock();
        }
};

#endif
